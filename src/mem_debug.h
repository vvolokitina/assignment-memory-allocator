#ifndef _MEM_DEBUG_H_
#define _MEM_DEBUG_H_
#include "mem_internals.h"

void debug_heap(FILE* f, void const* ptr);

#endif
