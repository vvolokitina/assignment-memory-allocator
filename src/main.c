#include <stdio.h>
#include <string.h>
#include "mem_internals.h"
#include "mem.h"
#include "mem_debug.h"

void print_test(char *const header) {
    static size_t test_n = 1;
    printf("\nTest %zu...................................\n%s\n ", test_n, header);
    test_n++;
}

int main() {
    print_test("Init heap");
    void *heap = heap_init(8192);
    debug_heap(stdout, heap);

    print_test("Successful memory allocation");
    void *block1 = _malloc(128);
    void *block2 = _malloc(512);
    _malloc(128);
    void *block3 = _malloc(1);
    _malloc(1024);
    debug_heap(stdout, heap);

    print_test("Free one block");
    _free(block3);
    debug_heap(stdout, heap);

    print_test("Free two blocks");
    _free(block2);
    _free(block1);
    debug_heap(stdout, heap);

    print_test("New region memory expands the old one");
    _malloc(256);
    _malloc(10000);
    _malloc(10256);
    _malloc(512);
    debug_heap(stdout, heap);
    return 0;
}
