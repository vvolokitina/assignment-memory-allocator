CFLAGS=--std=c18 -Isrc/ -pedantic -Wall -Werror -DDEBUG
BUILDDIR=build/
SRCDIR=src/
CC=gcc

APP=$(BUILDDIR)/allocator

C_SRC_FILES=$(wildcard $(SRCDIR)*.c)
OBJ_FILES=$(addprefix $(BUILDDIR),$(notdir $(C_SRC_FILES:.c=.o)))

define build-obj
	$(CC) -c $(CFLAGS) $< -o $@
endef

all: $(APP)

$(APP): $(OBJ_FILES)
	$(CC) -o $(APP) $^

$(OBJ_FILES): $(BUILDDIR)%.o: $(SRCDIR)%.c
	mkdir -p $(BUILDDIR)
	$(call build-obj)

clean:
	rm -rf $(BUILDDIR)

